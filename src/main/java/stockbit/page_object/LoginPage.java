package stockbit.page_object;

import org.openqa.selenium.By;

public class LoginPage extends BasePage {

    public void isOnboardingPage(){
        assertIsDisplay(By.id("com.stockbit.android:id/ivWellcomeImageHeader"));
    }
    public void tapLogin(){
        tap(By.id("com.stockbit.android:id/btnWellcomeLogIn"));
    }


    public void inputUsername(String username){
        typeOn(By.xpath("//android.widget.EditText[@resource-id=\"com.stockbit.android:id/tiet_text_field_input\" and @text=\"Username or Email\"]"), username);

    }

    public void inputPassword(String password){
        typeOn(By.xpath("//android.widget.EditText[@resource-id=\"com.stockbit.android:id/tiet_text_field_input\" and @text=\"Password\"]"), password);

    }

    public void tapLoginButton(){
        tap(By.id("com.stockbit.android:id/tv_login_submit"));
    }

    public void tapSkipBiometric(){
        tap(By.id("com.stockbit.android:id/btn_smart_login_skip"));
    }

    public void tapSkipAvatar(){
        tap(By.id("com.stockbit.android:id/btn_skip_choose_avatar"));
    }

    public void isWatchlistPage(){
        tapSkipBiometric();
        tapSkipAvatar();
        assertIsDisplay(By.xpath("//android.widget.TextView[@resource-id=\"com.stockbit.android:id/tv_name\" and @text=\"All Watchlist\"]"));
    }


}
