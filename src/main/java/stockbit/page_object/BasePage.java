package stockbit.page_object;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import stockbit.android_driver.AndroidDriverInstance;

public class BasePage {
    public AndroidDriver driver(){
        return AndroidDriverInstance.androidDriver;
    }

    public void tap(By element){
        driver().findElement(element).click();
    }

    public void typeOn(By element, String text){
        driver().findElement(element).sendKeys(text);
    }

    public boolean isDisplayed(By element){
        return driver().findElement(element).isDisplayed();
    }
    public void assertIsDisplay(By element){
        if (!isDisplayed(element)){
            throw new AssertionError(String.format("This element '%s' not found", element));
        }
    }
}
