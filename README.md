# android-automation-2-1



## Overview

Project ini berisi tes otomatisasi untuk aplikasi Android Stockbit. Dibangun menggunakan IntelliJ IDEA, project ini memanfaatkan berbagai dependensi untuk menjalankan tes otomatisasi.

## Dependencies

### Appium

Deskripsi: Tools open-source untuk mengotomatisasi aplikasi mobile.

Penggunaan: Digunakan untuk otomatisasi tes mobile pada proyek ini.

### Cucumber
Deskripsi: Framework tes yang mendukung Behavior-Driven Development (BDD), memungkinkan penulisan kasus tes dalam format bahasa alami.

Penggunaan: Digunakan untuk mendefinisikan skenario tes dalam bentuk Gherkin.

### JUnit
Deskripsi: Framework JUnit untuk Java yang banyak digunakan untuk unit testing.

Penggunaan: Menggunakan kelas dan anotasi JUnit untuk menulis dan menjalankan tes.

### Selenium
Deskripsi: Alat otomatisasi untuk aplikasi web.

Penggunaan: Menggunakan kelas dan metode Selenium untuk otomatisasi dalam proyek Java.

## Setup Instructions

### 1. Install Appium
Perintah : ``` npm install -g appium ```

### Appium Inspector

Konfigurasi Desired Capability:


```
{
  "platformName": "Android",
  "appium:platformVersion": "12",
  "appium:automationName": "UiAutomator2",
}
```

### 2. Cucumber Setup
Langkah :

1. Buka file build.gradle.
2. Tambahkan dependensi berikut:
```
dependencies {
    implementation 'io.cucumber:cucumber-java:7.3.0'
    implementation 'io.cucumber:cucumber-junit:7.3.0'
    testImplementation 'junit:junit:4.13.1'
}
```
### 2. JUnit Setup
Langkah : 

1. Buka file build.gradle. 
2. Tambahkan dependensi berikut:

```
dependencies {
    testImplementation 'junit:junit:4.13.1'
}
```

### 3.Selenium Setup
Langkah:

1. Buka file build.gradle.
2. Tambahkan dependensi berikut:

```
dependencies {
testImplementation 'org.seleniumhq.selenium:selenium-java:3.141.59'
}
```
## How To Run Automation
### 1. Run Appium

Perintah: appium

### 2. Open Simulator in Android Studio
   
Langkah:
Pastikan perangkat virtual terinstal dan tekan tombol play.
Pastikan sesi sudah terhubung dengan Appium.

### 3. Running the Test

File: Login.feature 
   
Langkah:
   Buka file tersebut di IntelliJ IDEA.
   Klik 'Run' pada 'Scenario: Success login'.